# Note common

This repository contains common code fragments that can be used by notes. In general, these fragments would be very useful to be maintained centrally.

In rare cases, when the notes have completely different flow they can overide or fully re-implement as per it's own needs.

When they reuse, we can continously improve the shared fragments without the need of changing in all notes individually and going through the release cycle.

## Release process and backward compatibility

This common fragments go through release process as well. So they are version tagged. This way, old notes that are not compatible with latest changes can have it's own 